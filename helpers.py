import discord, datetime
from discord.utils import get
from asyncio import sleep
from config import *
from discord import Intents
from discord.ext import commands

intents = Intents.all()
bot = commands.Bot(command_prefix='!', intents=intents)
bot.launch_time = datetime.datetime.utcnow()

async def add_role(user_id, role_name):
    role = get(bot.guilds[0].roles, name=role_name)
    user = get(bot.guilds[0].members, id=user_id)
    await user.add_roles(role)
    print(f"Role '{role_name}' added to '{user}'.")

async def remove_role(user_id, role_name):
    role = get(bot.guilds[0].roles, name=role_name)
    user = get(bot.guilds[0].members, id=user_id)
    await user.remove_roles(role)
    print(f"Role '{role_name}' removed from '{user}'.")

async def logsend(user_message, content, colour):
    debug = discord.Embed(title=content, color=colour)
    debug = await user_message.channel.send(embed=debug)
    print(content)

async def get_message(channel_id, message_id):
    channel = bot.get_channel(channel_id)
    message = await channel.fetch_message(message_id)
    return message
    print("Message {message_id} retrived and has following content {message.content}.")
