from helpers import *
import json
from emoji import emojize

try:
	roles = json.loads(open("roles.json").read())
except:
	roles = []

@bot.command()
@commands.has_permissions(administrator=True)
async def newrole(ctx, emoji, name, message_id, channel_id):
    """Makes a role self-assignable, requires: emoji, role name, message_id (reaction message) and channel_id (channel in which the message is)."""
    # Convert the discord emoji to a unicode emoji (required)
    emoji = emojize(emoji)

	# Create a new "role" entry
    roles.append({
        "emoji": emoji,
        "name": name,
        "message_id": int(message_id),
        "channel_id": int(channel_id)
    })
    open("roles.json", "w").write(json.dumps(roles, indent=4))

    reaction_message = await get_message(int(channel_id), int(message_id))
    await reaction_message.add_reaction(emoji)

    await logsend(ctx.message, "New self-assign role is created successfully.", discord.Colour.green())

@bot.command()
@commands.has_permissions(administrator=True)
async def delrole(ctx, name):
    """Requires the role name, does the opposite of !newrole."""
    for role in roles:
        if role["name"] == name:
            reaction_message = await get_message(int(role["channel_id"]), int(role["message_id"]))
            await reaction_message.clear_reaction(role["emoji"])
            roles.remove(role)
    await logsend(ctx.message, "Self-assign role is removed successfully.", discord.Colour.green())

@bot.event
async def on_raw_reaction_add(reaction):
    for role in roles:
        if reaction.message_id == role["message_id"]:
            if str(reaction.emoji) == role["emoji"]:
                await add_role(reaction.user_id, role["name"])

@bot.event
async def on_raw_reaction_remove(reaction):
    for role in roles:
        if reaction.message_id == role["message_id"]:
            if str(reaction.emoji) == role["emoji"]:
                await remove_role(reaction.user_id, role["name"])
