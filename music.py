from helpers import *
import youtube_dl

ytdl_format_options = { 'format': 'bestaudio/best' }
ytdl = youtube_dl.YoutubeDL(ytdl_format_options)

class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data
        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None):
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=False))

        filename = data['url']
        ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename), data=data)

@bot.command()
async def play(ctx, url):
    """Plays a song from YouTube (from its url)."""
    if ctx.voice_client is None:
        if ctx.author.voice:
            await ctx.author.voice.channel.connect()
        else:
            await ctx.send("You are not connected to a voice channel.")
    elif ctx.voice_client.is_playing():
        ctx.voice_client.stop()

    async with ctx.typing():
        player = await YTDLSource.from_url(url, loop=bot.loop)
        ctx.voice_client.play(player, after=lambda e: print(f'Player error: {e}') if e else None)

    await ctx.send(f'Now playing: `{player.title}`.')

@bot.command()
async def stop(ctx):
    """Stops and disconnects the bot from the voice channel."""
    await ctx.voice_client.disconnect()
    await ctx.send("Disconnected from voice.")
